<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\LevelSetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->paths([
        __DIR__ . '/afficher_diff',
        __DIR__ . '/base',
        __DIR__ . '/formulaires',
        __DIR__ . '/genie',
        __DIR__ . '/inc',
        __DIR__ . '/prive',
		__DIR__ . '/revisions_administration.php',
		__DIR__ . '/revisions_autoriser.php',
		__DIR__ . '/revisions_ieconfig.php',
		__DIR__ . '/revisions_pipeline.php',
    ]);

	$rectorConfig->sets([
		LevelSetList::UP_TO_PHP_81
	]);
};
